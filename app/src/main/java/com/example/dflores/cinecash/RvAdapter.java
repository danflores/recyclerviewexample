package com.example.dflores.cinecash;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by dflores on 04/04/2017.
 */
public class RvAdapter extends RecyclerView.Adapter<RvAdapter.MovementsVH> {
    List<Transactions> transactionsList;

    RvAdapter(List<Transactions> transactionsList){
        this.transactionsList = transactionsList;
        Log.e("RvAdapter","transactionlist size --- "+transactionsList.size());
    }

    @Override
    public MovementsVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.transactions,parent,false);
        //ButterKnife.bind(this,v);
        Log.e("RvAdapter","transactionlist dddd size");
        MovementsVH movementsVH = new MovementsVH(v);
        return movementsVH;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onBindViewHolder(MovementsVH holder, int position) {
        Log.e("RvAdapter","transactionlist size "+transactionsList.size());
        if(transactionsList !=null && transactionsList.size()>0) {
            Transactions transactions = transactionsList.get(position);
            holder.date.setText(dateFormat(transactions.getDate()));
            holder.description.setText(transactions.getDescription());
            holder.quantity.setText(""+transactions.getCredits());
        }
    }

    @Override
    public int getItemCount() {
        if(transactionsList!=null && transactionsList.size()>0) {
            return transactionsList.size();
        }else{
            return 0;
        }
    }


    public static class MovementsVH extends RecyclerView.ViewHolder{
        CardView cv;
        TextView date;
        TextView description;
        TextView quantity;


        public MovementsVH(View itemView) {
            super(itemView);
            Log.e("RvAdapter","transactionlist siz....e");

            cv = (CardView) itemView.findViewById(R.id.movements_cv);
            date = (TextView) itemView.findViewById(R.id.date);
            description = (TextView) itemView.findViewById(R.id.description);
            quantity = (TextView) itemView.findViewById(R.id.quantity);
        }
    }

    public String dateFormat(String date){
        String dateFormat = null;
        Date d;
        SimpleDateFormat f = new SimpleDateFormat("d M yyyy");
        Locale locale = new Locale("es","MX");
        DateFormat dateFormat1 = DateFormat.getDateInstance(DateFormat.LONG,locale);
        try{
            if(date!=null && !date.isEmpty()) {
                d = f.parse(date);
                Log.e("Transactions","fecha2 "+d.toString());
                dateFormat = dateFormat1.format(d);
            }
        }catch (Exception e){
            dateFormat = dateFormat1.format(new Date());
        }
        return  dateFormat;
    }
}
