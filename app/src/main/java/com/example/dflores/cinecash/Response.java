package com.example.dflores.cinecash;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by dflores on 04/04/2017.
 */
public class Response {
    @SerializedName("credits_available")
    private double credits_available;
    @SerializedName("transactions")
    private List<Transactions> transactions;

    Response(double credits_available,List<Transactions> transactions){
        this.credits_available = credits_available;
        this.transactions = transactions;
    }
    public double getCredits_available() {
        return credits_available;
    }

    public void setCredits_available(double credits_available) {
        this.credits_available = credits_available;
    }

    public List<Transactions> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transactions> transactions) {
        this.transactions = transactions;
    }
}
