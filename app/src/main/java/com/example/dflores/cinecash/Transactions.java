package com.example.dflores.cinecash;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by dflores on 04/04/2017.
 */
public class Transactions {
    @SerializedName("action")
    private String action;
    @SerializedName("concept")
    private  String description;
    @SerializedName("credits")
    private double credits;
    @SerializedName("date")
    private String date;

    public Transactions(String action, String description, double credits, String date){
        this.action = action;
        this.description = description;
        this.credits = credits;
        this.date = date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getCredits() {
        return credits;
    }

    public void setCredits(double credits) {
        this.credits = credits;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


}
