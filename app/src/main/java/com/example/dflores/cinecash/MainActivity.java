package com.example.dflores.cinecash;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends Activity {

    private Response response;
   /* @BindView (R.id.name) TextView name;
    @BindView (R.id.email) TextView email;
    @BindView (R.id.credit) TextView credit;
    @BindView (R.id.rview) RecyclerView rview;*/
    TextView credit;
    RecyclerView rview;
    TextView without_transactions;
    private final static String API_KEY="cinepolis_test_android";
    private final static String AUTHORIZATION = "bearer DsnMY1YrzbhKfIOb8NM-R_U8-PvY-LKUBypaPpjOe-OYObN8ZBaYj0l1Iizk034ny9V6D16SmIbfv3tApxb4qqe7Vxjpin2kCshHeBb_0TO6R6XRIm_Hm1M0bEEnyxIeTRanvhWunfvFCQkkZFESjnRTJpJjibtsY-pYC2e-InYFiwOAvIbb-jC7n3cThChS_Q_TTkFr_uh0bUnziN8fkgGhmamNgP9WwZx4kiK45625wrChOHIQQcCnjuHyg3VKiD5ZwmWQJxLshy2lLeTSF5XRKh4ZvrQnNWPSRfqL4AgsQ-y8uxLj4K2oC0yw9DLzHIYqvd7Z6r19dwzIMqTVcbE2CNd4P54_nQUFu0vLRQEF1GY2Khtjch6ZWw4RsIa6NYcWdw";

    private List<Transactions> transactionsList;
    private double credits_available;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // ButterKnife.bind(this);
        rview = (RecyclerView) findViewById(R.id.rview);
        credit = (TextView) findViewById(R.id.credit);
        without_transactions = (TextView) findViewById(R.id.withoutTrans);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rview.setLayoutManager(llm);
        initializeData();
    }

    private void initializeAdapter() {
        RvAdapter adapter = new RvAdapter(transactionsList);
        rview.setAdapter(adapter);
    }

    public void initializeData(){
        transactionsList = new ArrayList<Transactions>();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Response> call = apiService.getResponse(API_KEY, AUTHORIZATION);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Log.e("Main","Success");
                Response r = new Response(response.body().getCredits_available(),response.body().getTransactions());
                credits_available = r.getCredits_available();
                credit.setText("$ "+credits_available);
                transactionsList = r.getTransactions();
                if(transactionsList!=null && transactionsList.size()>0){
                    without_transactions.setVisibility(View.GONE);
                    rview.setVisibility(View.VISIBLE);
                    initializeAdapter();
                }else{
                    without_transactions.setVisibility(View.VISIBLE);
                    rview.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {

                Log.e("Main","Fallo al consultar servicio "+t.toString());
                if(transactionsList!=null && transactionsList.size()>0){
                    without_transactions.setVisibility(View.GONE);
                    rview.setVisibility(View.VISIBLE);
                    initializeAdapter();
                }else{
                    without_transactions.setVisibility(View.VISIBLE);
                    rview.setVisibility(View.GONE);
                }
            }
        });

        /*transactionsList.add(new Transactions("Compra","Película: Power Rangers",26,"2017-03-29T16:19:17"));//Compra
        transactionsList.add(new Transactions("Compra","Película: Final Apertura LLN League of Legends.",200,"2017-03-25T19:43:48"));//Compra
        transactionsList.add(new Transactions("TransferenciaEnvio","Recarga con tarjeta bancaria: 548234XXXXXX5182.",50,"2017-01-27T12:32:26"));//Transferencia (Recarga)
        transactionsList.add(new Transactions("TransferenciaRecibe","Créditos para: daniela.polea@gmail.com.",500,"2016-12-19T12:55:25"));//Transferencia Envio (Se restan creditos)
        transactionsList.add(new Transactions("TransferenciaPayPal","TransferenciaPayPal",300,"2016-12-09T13:18:49")); //Transferencia PayPal (Se suman creditos)*/
    }
}
