package com.example.dflores.cinecash;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dflores on 04/04/2017.
 */
public class Movements {
    private int action;
    private String date;
    private String description;
    private double quantity;


    Movements(int action,String date,String description,double quantity){
        this.action = action;
        this.date = date;   //darle formato a la fecha
        this.description = description;
        this.quantity = quantity;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

}
