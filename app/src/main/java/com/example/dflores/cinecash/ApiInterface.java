package com.example.dflores.cinecash;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by dflores on 05/04/2017.
 */

public interface ApiInterface {
    @GET("v1/members/cinecash?country_code=MX")
    Call<Response> getResponse(@Header("api_key") String apiKey, @Header("Authorization") String autorizathion);

}
